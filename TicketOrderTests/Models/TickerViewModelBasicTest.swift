//
//  TickerViewModelBasicTest.swift
//  TicketOrderTests
//
//  Created by Sun, Randy on 27/10/19.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation
import UIKit
import Nimble
import XCTest

@testable import TicketOrder

class TickerViewModelTests: XCTestCase {
    
    var viewModel = TicketViewModel()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewModelDataLoading() {
        //Todo: here should setup http mockup to get resposne
        //Then call viewModel.loadTicket()
    }
    
    func testTableCellType() {

        let firstCell = viewModel.getTableCellType(indexPath: IndexPath(row: 0, section: 0))
        expect(firstCell).toEventually(equal(TicketCellIndex.inputCell), timeout: 10) //This is used for test network call or need time waiting, actually we don't need it here, but for future test network api, so leave it here
        
        let secondCell = viewModel.getTableCellType(indexPath: IndexPath(row: 1, section: 0))
        expect(secondCell).toEventually(equal(TicketCellIndex.stopLoss), timeout: 10)
    }
    
    func testCellCount() {

        let cellCount = viewModel.getCellCount()
        expect(cellCount).to(equal(3))
        
    }
    
    func testViewModelString() {
        let buyText = viewModel.buyText
        expect(buyText).to(equal("Buy"))
        let sellText = viewModel.sellText
        expect(sellText).to(equal("Sell"))
        let cancelText = viewModel.cancelText
        expect(cancelText).to(equal("Cancel"))
        let confirmText = viewModel.confirmText
        expect(confirmText).to(equal("Confirm"))
        let stopLossText = viewModel.stopLossText
        expect(stopLossText).to(equal("Stop loss"))
        let takeProfiltText = viewModel.takeProfiltText
        expect(takeProfiltText).to(equal("Take profit"))
        let marginText1 = viewModel.marginText1
        expect(marginText1).to(equal("Margin example 1"))
        let marginText2 = viewModel.marginText2
        expect(marginText2).to(equal("Margin example 2"))
    }

    
}
