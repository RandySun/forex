//
//  Constants.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 20/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation


enum Constants {

    enum Authentication {
        static let authorisationKey = "Authorization"
        static var accessToken = ""
        static let registrationCodeLength = 6
    }
    
    enum Headers {
        static let fundCodeKey = "fundCode"
        static let correlationId = "correlationId"
        static let subscriptionKey = "Ocp-Apim-Subscription-Key"

        static let contentType = "Content-Type"

        static let buildVersion = "mobileApp-build-version"

        enum ContentType {
            static let json = "application/json"

            static func formData(boundary: String) -> String {
                return "multipart/form-data; boundary=\(boundary)"
            }
        }
    }


    enum UserDefaults {
        static let isBiometricsEnabled = "isBiometricsEnabled"
        static let isPINEnabled = "isPINEnabled"
        static let hasEnabledBiometricsBefore = "hasEnabledBiometricsBefore"
        static let hasLaunchedBefore = "hasLaunchedBefore"
        static let termsAcceptedForNewUser = "termsAcceptedForNewUser"
    }


    enum Animations {
        static let splash = "splashAnimation"
        static let loading = "loadingAnimation"
        static let success = "successAnimation"
    }

    enum AuthorisationCode {
        static let wrongCredentialCode = 1
        static let accountLockOut = 2
    }

    enum LocalAuthentication {
        static let maximumAllowablePINOrBiometricsAttempts = 3
    }

    enum SessionTimeout {
        static let maximumAllowableSecondsInBackground: Double = 300.0
    }

    enum AnalyticsActionType {
        case tapped
        case accepted
        case cancelled
    }

    enum LinkType {
        case download
        case open
        case system
    }
    
    
    enum Paginition {
        static let pageNumber = 5
    }
    
}
