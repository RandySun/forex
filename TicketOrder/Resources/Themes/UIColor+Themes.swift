//
//  UIColor+Themes.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 17/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

/**
 An extension on UIColor that contains an enumeration of all custom colors, including those that vary with
 theme, and contains a convenience initializer that returns a corresponding color.
 */
extension UIColor {

    /// An enumeration of all color set names, including those that vary according to theme.
    enum name: String {
        case primaryDark
        case primaryLight
        case accentDark
        case accentLight
        case textFieldBorder

        case navy
        case disabled
        case textFieldText
        case backgroundLight
        case divider
        case backgroundLightGrey
        case tabBarTint
        case nightBlue

        case updateBG
        case updateText

        case splashAccent
    }

    /** Convenience initializer that returns force unwrapped UIColor for a theme given an enumerated color
     name.
     - Parameter name: The enumerated UIColor name

     Note that because the color that is returned is force unwrapped, a runtime crash will occur if there is no
     color set with the corresponding color name in the asset catalog (.xcassets) used for the target.
     */
    convenience init(named name: UIColor.name) {
        self.init(named: name.rawValue)!
    }
}
