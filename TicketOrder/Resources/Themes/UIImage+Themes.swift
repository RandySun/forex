//
//  UIImage+Themes.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 17/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

/**
 An extension on UIImage that contains an enumeration of all images, including those that vary with
 theme, and contains a convenience initializer that returns a corresponding image.
 */
extension UIImage {

    /// An enumeration of all image set names, including that vary according to theme.
    enum name: String {
        // Logo
        case logo

        // General
        case close
        case rightArrow
        case chevronRight
        case chevronDown
        case downloadArrow
        case systemlinkArrow

        // Navigation Bar
        case navigationBar
        case backButton
        case navi5s
        case navi6
        case navi6s
        case navixr
        case navixs

        // Tab Bar
        case tabBar
        case tabBarItemHome
        case tabBarItemNotifications
        case tabBarItemHamburger

        // Background
        case backgroundLogin

        // Web View
        case webViewBack
        case webViewForward
        case webViewRefresh

        // Login
        case passwordHidden
        case passwordShown

        // PIN Login
        case lockWhite
        case pinFilledLight

        // Security Settings
        case pinEmpty
        case pinFilled
        
        // Claim History
        case filter
        
        // Notifications
        case remove
        case tabBarItemNotificationsSelected
        case tabBarItemNotificationsDotSelected
        case tabBarItemNotificationsDotUnselected
        
        // Cover Details
        case coverDetailsViewCoverPDS

        // Table View Cell
        case tableViewCellRoute
        case tableViewCellExternalLink
        case tableViewCellDownload

        // Home
        case backgroundHome
        case homeTableViewCellMakeClaim
        case homeTableViewCellExtrasLimit
        case homeTableViewCellCheckCover
        case homeTableViewCellClaimHistory
        case homeNotificationExclamation
        case homeNotificationClose

        // Locate A Centre
        case phone

        // Extras
        case extrasDental
        case extrasOptical
        case extrasTherapies
        case extrasHealthyLifestyle
        case extrasPharmaceutical
        case extrasAmbulance
        case extrasArtificialAids
        case extrasMedicalTravel
    }

/** Convenience initialiser that returns force unwrapped UIImage for a theme given an enumerated image name.
     - Parameter name: The enumerated UIImage name

     Note that because the image that is returned is force unwrapped, a runtime crash will occur if there is no image set with the corresponding image name in the asset catalog (.xcassets) used for the target.
     */
    convenience init(named name: UIImage.name) {
        self.init(named: name.rawValue)!
    }
}
