//
//  AppDelegate.swift
//  TicketOrder
//
//  Created by Sun, Randy on 20/10/19.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

}

