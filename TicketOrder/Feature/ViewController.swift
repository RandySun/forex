//
//  ViewController.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {

    //TableView
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
   
    //Header
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var heightConstraintDisabled: NSLayoutConstraint!
    let titleView: BuySellTitleView = BuySellTitleView.fromNib()
    var pickerView = UIPickerView()
    let buySellView: BuySellView = BuySellView.fromNib()
    
    //Footer
    @IBOutlet weak var confirmButton: TertiaryLink!
    @IBOutlet weak var cancelButton: TertiaryLink!
    @IBOutlet weak var footerView: UIView!
    
    var viewModel = TicketViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadTicket()
        viewModel.delegate = self
        setupHeader()
        setupTableView()
        setupUIStyle()
        setupFooter()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func setupUIStyle() {
        self.title = "Ticker Example"
               switch GlobalSettings.background {
        case .dark:
            view.backgroundColor = .black
            tableView.backgroundColor = .black
            refreshControl.tintColor = .white
            footerView.backgroundColor = .black
            cancelButton.backgroundColor = .gray
            confirmButton.backgroundColor = UIColor(named: .navy)
            confirmButton.setTitleColor(.white, for: .normal)
            cancelButton.setTitleColor(.white, for: .normal)
            
        case .light:
            view.backgroundColor = .white
            tableView.backgroundColor = .white
            refreshControl.tintColor = .black
            footerView.backgroundColor = .white
            cancelButton.backgroundColor = .gray
            confirmButton.backgroundColor = UIColor(named: .navy)
            confirmButton.setTitleColor(.white, for: .normal)
            cancelButton.setTitleColor(.black, for: .normal)
        
        }
    }
    
    func setupHeader() {
        
        heightConstraintDisabled.isActive = false
        headerView.stackViewsVerticallyWithHorizontalPadding([titleView, buySellView], horizontalPadding: 0)
        titleView.sizeToFit()
        buySellView.configurationButtonAction(buyAction: {
            self.viewModel.operationType = .buy
            self.titleView.buyLabel.text = self.viewModel.buyText
            self.updateBy(currency: self.viewModel.currencyType , type: .market, operatonType: .buy)
        }, sellAction: {
            self.viewModel.operationType = .sell
            self.titleView.buyLabel.text = self.viewModel.sellText
            self.updateBy(currency: self.viewModel.currencyType , type: .market, operatonType: .sell)
        } ,levelAction: {
            //Todo: show level contents
        })
        buySellView.sizeToFit()
        headerView.sizeToFit()
        titleView.currencySelector.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        buySellView.buyHandler()
        headerView.layoutIfNeeded()
    }
    
    func setupFooter() {
        cancelButton.setTitle(title: viewModel.cancelText)
        confirmButton.setTitle(title: viewModel.confirmText)
        cancelButton.addRoundedCorners()
        confirmButton.addRoundedCorners()
        confirmButton.isEnabled = false //by default, this is disabled unless correct values inputted
    }

    //This function handle all kinds of UI update accoring the different inputs, refresh
    func updateBy(currency: Currency, type: Type, operatonType: OperationType) {
        //Update videModel
        self.viewModel.currencyType = currency
        self.viewModel.operationType = operatonType
        self.viewModel.marketType = type
        viewModel.displayedTicket = viewModel.ticket?.getTicket(by: currency)
        
        //Update Header
        titleView.titleLabel.text = "AUD / " + currency.rawValue
        titleView.buyLabel.text = (.buy == operatonType) ? viewModel.buyText : viewModel.sellText
        guard let displayedTicket = viewModel.displayedTicket else {  return }
        
        if let buyPriceString = displayedTicket.getRateDetail(by: currency)?.buyTT, let buyPrice = Float(buyPriceString), let sellPriceString = displayedTicket.getRateDetail(by: currency)?.sellTT, let sellPrice = Float(sellPriceString) {
            
            buySellView.update(buyPrice: buyPrice, sellPrice: sellPrice)
            
            //Update cell
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? InputCell {
                switch operatonType {
                case .buy:
                    if let priceString = displayedTicket.getRateDetail(by: currency)?.buyTT, let price = Float(priceString) {
                        cell.price = price
                    }
                case .sell:
                    if let priceString = displayedTicket.getRateDetail(by: currency)?.sellTT, let price = Float(priceString) {
                        cell.price = price
                    }
                }
                
                if cell.getUnits() > 0 {
                    cell.updateTextField(cell.unitTextField)
                }
            }
        } else {
            
            buySellView.update(buyPrice: -1, sellPrice: -1) // Update to NA in buy sell view
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? InputCell {
                cell.price = -1.0 // update to NA in input cell 
                cell.updateTextField(cell.unitTextField)
            }
            
        }
   
    }

}


