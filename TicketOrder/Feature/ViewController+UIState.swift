//
//  ViewController+UIState.swift
//  TicketOrder
//
//  Created by Sun, Randy on 25/10/19.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation
import UIKit


extension ViewController: ViewModelDelegate {
    
    func update(for state: ViewModelState) {
        
        switch state {
        case .loading:
            self.refreshControl.beginRefreshing()
        default:
            self.refreshControl.endRefreshing()
        }
        
        switch state {
            
        case .loading:
            refreshControl.isEnabled = false
            
        case .initial:

            break
            
        case .loaded:
            self.updateBy(currency: viewModel.currencyType, type: self.viewModel.marketType, operatonType: self.viewModel.operationType)
            tableView.reloadData()
            
        case let .error(err):
            
            debugPrint("Refreshing getting error:\(err.localizedDescription)")
            
            break
            
        case let .apiError(err):
            
            debugPrint("Refreshing getting API error:\(err.localizedDescription)")
            break
            
        }
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        self.refreshControl.beginRefreshing()
        viewModel.loadTicket()
    }
    
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Currency.getAllCurrencies().count//self.viewModel.ticket?.getCurrencyCount() ?? 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let currencies = Currency.getAllCurrencies()
        guard row < currencies.count else {
            return "NA"
        }
        return currencies[row].rawValue
    }

     
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let currencies = Currency.getAllCurrencies()
        guard row < currencies.count else {
            titleView.titleLabel.text = "NA"
            dismissKeyboard()
            return
        }
        //clearInputsBasedOnCurrencyChange()
        updateBy(currency: Currency.getCurrency(currency: currencies[row].rawValue), type: self.viewModel.marketType, operatonType: self.viewModel.operationType)
        dismissKeyboard()
    }
    
    private func clearInputsBasedOnCurrencyChange() {
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? InputCell {
            cell.unitTextField.text = ""
            cell.amountTextField.text = ""
        }
    }
    
    
}


