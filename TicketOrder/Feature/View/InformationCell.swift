//
//  InformationCell.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//
import UIKit

class InformationCell: UITableViewCell, TableViewCellLoadsFromNib {

    @IBOutlet weak var localImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    private func setupUI() {
        backgroundColor = .clear
        titleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        
        switch GlobalSettings.background {
            
        case .light:
            backgroundColor = UIColor.white
            titleLabel.textColor = UIColor(named: .navy)
            imageView?.tintColor = UIColor(named: .navy)
        case .dark:
            backgroundColor = UIColor.black
            titleLabel.textColor = UIColor.white
            imageView?.tintColor = UIColor.white
        }
    }

    public func update(
        title: String,
        image: UIImage? = nil) {

        titleLabel.text = title
        localImageView.image = image
        

    }
}
