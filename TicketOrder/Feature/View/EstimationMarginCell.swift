//
//  EstimationMarginCell.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import UIKit

class EstimationMarginCell: UITableViewCell, TableViewCellLoadsFromNib {
    
    @IBOutlet weak var titleViewContainer: UIView!
    @IBOutlet weak var localImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var subViewContainer: UIView!
    var expanded = false
    var subViews: [UIView] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .clear
        titleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        subTitleLabel.font = ScaledFont.shared.font(forTextStyle: .callout)
        
        switch GlobalSettings.background {
            
        case .light:
            backgroundColor = .white
            subViewContainer.backgroundColor = .white
            titleViewContainer.backgroundColor = .white
            titleLabel.textColor = UIColor(named: .navy)
            subTitleLabel.textColor = UIColor(named: .navy)
        case .dark:
            backgroundColor = .black
            subViewContainer.backgroundColor = .black
            titleViewContainer.backgroundColor = .black
            titleLabel.textColor = .white
            subTitleLabel.textColor = .white
        }
        
    }
    
    public func update(
        title: String,
        subTitle: String,
        subViews: [UIView]) {
        
        titleLabel.text = title
        subTitleLabel.text = subTitle
        self.subViews = subViews
    }
    
    public func onClick() {
        expanded = !expanded
        
        
        //Todo: Update image view image as wll
        if !expanded {
            let _ = subViewContainer.subviews.map{
                $0.removeFromSuperview()
            }
        }
        
        if expanded && subViewContainer.subviews.count == 0 {
            subViewContainer.stackViewsVerticallyWithHorizontalPadding(subViews, horizontalPadding: 0.0)
        }
        
        forceToUpdateCell()
    }
    
    private func forceToUpdateCell() {
        subViewContainer.invalidateIntrinsicContentSize()
        subViewContainer.setNeedsLayout()
        subViewContainer.sizeToFit()
        subViewContainer.layoutSubviews()
        self.invalidateIntrinsicContentSize()
        self.setNeedsLayout()
        self.sizeToFit()
    }
}
