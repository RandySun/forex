//
//  GeneralDescriptionView.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//
import UIKit

class GeneralDescriptionView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    private func setupUI() {
        backgroundColor = .clear
        titleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        subTitleLabel.font = ScaledFont.shared.font(forTextStyle: .callout)
        
        switch GlobalSettings.background {
            
        case .light:
            backgroundColor = UIColor.white
            titleLabel.textColor = UIColor(named: .navy)
            subTitleLabel.textColor = UIColor(named: .navy)
        case .dark:
            backgroundColor = UIColor.black
            titleLabel.textColor = .white
            subTitleLabel.textColor = .white
        }
        
    }

    public func update(
        title: String,
        subTitle: String) {

        titleLabel.text = title
        subTitleLabel.text = subTitle
        layoutIfNeeded()

    }
}
