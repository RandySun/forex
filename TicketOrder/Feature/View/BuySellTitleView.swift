//
//  BuySellTitleView.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//
import UIKit

class BuySellTitleView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var currencySelector: UITextField!
    @IBOutlet weak var marketLabel: UILabel!
    @IBOutlet weak var buyLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .clear
        
        titleLabel.font = ScaledFont.shared.font(forTextStyle: .title2)
        marketLabel.font = ScaledFont.shared.font(forTextStyle: .callout)
        buyLabel.font = ScaledFont.shared.font(forTextStyle: .callout)
        
        switch GlobalSettings.background {
            
        case .light:
            
            backgroundColor = UIColor.white
            marketLabel.textColor = UIColor.gray
            titleLabel.textColor = UIColor(named: .navy)
            buyLabel.textColor = UIColor.gray
            
        case .dark:
            backgroundColor = UIColor.black
            marketLabel.textColor = UIColor.white
            titleLabel.textColor = UIColor.white
            buyLabel.textColor = UIColor.white
        }
        
    }
    
    public func update(
        title: String,
        market: String? = nil,
        buy: String? = nil) {
        
        titleLabel.text = title
        
        
    }
}
