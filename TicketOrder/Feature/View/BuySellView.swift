//
//  BuySellView.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//


import UIKit


class BuySellView: UIView {
    
    @IBOutlet weak var sellContainerView: UIView!
    @IBOutlet weak var sellTitleLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!
    @IBOutlet weak var sellLowLabel: UILabel!
    
    @IBOutlet weak var buyContainerView: UIView!
    @IBOutlet weak var buyTitleLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var buyLowLabel: UILabel!
    
    @IBOutlet weak var levelContainerView: UIView!
    @IBOutlet weak var levelTitleLabel: UILabel!
    
    var buyAction: (() -> ())?
    var sellAction: (() -> ())?
    var levelAction: (() -> ())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .clear
        
        sellTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        sellPriceLabel.font = ScaledFont.shared.font(forTextStyle: .title2)
        sellLowLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        
        buyTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        buyPriceLabel.font = ScaledFont.shared.font(forTextStyle: .title2)
        buyLowLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        
        levelTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        
        levelContainerView.roundCorners([.bottomLeft, .bottomRight], radius: 5)
        
        switch GlobalSettings.background {
            
        case .light:
            
            sellContainerView.backgroundColor = UIColor.white
            sellTitleLabel.textColor = UIColor(named: .textFieldText)
            sellPriceLabel.textColor = .black
            sellLowLabel.textColor = UIColor.gray
            
            buyContainerView.backgroundColor = UIColor.white
            buyTitleLabel.textColor = UIColor(named: .textFieldText)
            buyPriceLabel.textColor = .black
            buyLowLabel.textColor = UIColor.gray
            
            levelContainerView.backgroundColor = UIColor.white
            levelTitleLabel.textColor = UIColor(named: .textFieldText)
            
        case .dark:
            sellContainerView.backgroundColor = UIColor.black
            sellTitleLabel.textColor = UIColor.orange
            sellPriceLabel.textColor = UIColor.green
            sellLowLabel.textColor = UIColor.gray
            
            buyContainerView.backgroundColor = UIColor.black
            buyTitleLabel.textColor = UIColor.cyan
            buyPriceLabel.textColor = UIColor.green
            buyLowLabel.textColor = UIColor.gray
            
            levelContainerView.backgroundColor = UIColor.black
            levelTitleLabel.textColor = UIColor.white
        }
        
        let buyTap = UITapGestureRecognizer(target: self, action: #selector(self.buyHandler))
        buyContainerView.addGestureRecognizer(buyTap)
        
        let sellTap = UITapGestureRecognizer(target: self, action: #selector(self.sellHandler))
        sellContainerView.addGestureRecognizer(sellTap)
        
        let levelTap = UITapGestureRecognizer(target: self, action: #selector(self.levelHandler))
        levelContainerView.addGestureRecognizer(levelTap)
    }
    
    public func configurationButtonAction(buyAction: (() -> ())?,
                                          sellAction: (() -> ())?,
                                          levelAction: (() -> ())?) {
        self.buyAction = buyAction
        self.sellAction = sellAction
        self.levelAction = levelAction
    }
    
    public func update(
        buyPrice: Float,
        sellPrice: Float) {
        
        if buyPrice < 0 || sellPrice < 0 {
            sellPriceLabel.text = "NA"
            buyPriceLabel.text = "NA"
        } else {
            sellPriceLabel.text = "\(sellPrice)"
            buyPriceLabel.text = "\(buyPrice)"
        }
        
        
        
    }
    
    
    @objc func buyHandler() {
        //Todo: consider two modes:light and dark
        buyContainerView.backgroundColor = UIColor(named: .navy)
        levelContainerView.backgroundColor = UIColor(named: .navy)
        switch GlobalSettings.background {
        case .dark:
            sellContainerView.backgroundColor = .black
            
        case .light:
            sellContainerView.backgroundColor = .white
        }
        buyAction?()
    }
    
    @objc func sellHandler() {
        //Todo: consider two modes:light and dark
        sellContainerView.backgroundColor = UIColor.brown
        levelContainerView.backgroundColor = UIColor.brown
        switch GlobalSettings.background {
        case .dark:
            buyContainerView.backgroundColor = .black
            
        case .light:
            buyContainerView.backgroundColor = .white
        }
        sellAction?()
    }
    
    @objc func levelHandler() {
        levelAction?()
    }
}

