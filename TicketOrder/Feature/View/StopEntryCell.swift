//
//  StopEntryCell.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//
import UIKit

class StopEntryCell: UITableViewCell, TableViewCellLoadsFromNib {
    
    @IBOutlet weak var uinitContainer: UIView!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var gtcLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .clear
        
        unitLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        inputTextField.font = ScaledFont.shared.font(forTextStyle: .title2)
        
        gtcLabel.font = ScaledFont.shared.font(forTextStyle: .callout)
        
        inputTextField.delegate = self
        
        
        switch GlobalSettings.background {
            
        case .light:
            
            uinitContainer.backgroundColor = UIColor.white
            unitLabel.textColor = UIColor(named: .navy)
            inputTextField.textColor = UIColor.black
            gtcLabel.textColor = UIColor(named: .navy)
            
        case .dark:
            uinitContainer.backgroundColor = UIColor.black
            unitLabel.textColor = UIColor.cyan
            inputTextField.textColor = UIColor.white
            
            gtcLabel.textColor = UIColor.cyan
        }
        
        
    }
    
    public func update(
        row: Int) {
        
    }
}

extension StopEntryCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}



