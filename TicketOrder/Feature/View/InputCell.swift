//
//  InputCell.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//
import UIKit

enum UnitAmount: Int {
    case unit = 1,
    amount = 2
}

//Should go to localisation
enum Type: String {
    case market = "Market",
    limit = "Limit",
    stopEntry = "Stop Entry"
    
    static func getIndex(from type: Type) -> Int {
        switch(type) {
        case .market:
            return 0
        case .limit:
            return 1
        case .stopEntry:
            return 2
        }
    }
    
    static func getString(from index: Int) -> String {
        switch(index) {
        case 0:
            return Type.market.rawValue
        case 1:
            return Type.limit.rawValue
        case 2:
            return Type.stopEntry.rawValue
        default:
            return Type.market.rawValue
        }
    }
}

class InputCell: UITableViewCell, TableViewCellLoadsFromNib {
    
    @IBOutlet weak var unitContainerView: UIView!
    @IBOutlet weak var unitTitleLabel: UILabel!
    @IBOutlet weak var unitTextField: UITextField!
    
    
    @IBOutlet weak var amountContainerView: UIView!
    @IBOutlet weak var amountTitleLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    
    
    @IBOutlet weak var typeContainerView: UIView!
    @IBOutlet weak var typeTitleLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var price: Float = 1.0
    var type: Type = .market
    
    //If Limit, one extra cell added
    var typeAction: (() -> ())?
    //If value filled, enable confirm button and later opeation
    var valueAction: ((_ enableConfirm: Bool ) -> ())?
    
    let floatFormatter = NumberFormatter()
    let intFormatter = NumberFormatter()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        floatFormatter.numberStyle = NumberFormatter.Style.decimal
        floatFormatter.minimumFractionDigits = 2
        floatFormatter.maximumFractionDigits = 2
        
        intFormatter.numberStyle = NumberFormatter.Style.decimal
        intFormatter.maximumFractionDigits = 0
        
    }
    
    private func setupUI() {
        backgroundColor = .clear
        
        unitTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        unitTextField.font = ScaledFont.shared.font(forTextStyle: .body)
        
        amountTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        amountTextField.font = ScaledFont.shared.font(forTextStyle: .body)
        
        typeTitleLabel.font = ScaledFont.shared.font(forTextStyle: .body)
        pageControl.addRoundedCorners()
        
        typeTitleLabel.roundCorners([.topLeft, .topRight], radius: 5)
        
        unitTextField.tag = UnitAmount.unit.rawValue
        amountTextField.tag = UnitAmount.amount.rawValue
        
        unitTextField.delegate = self
        amountTextField.delegate = self
        
        switch GlobalSettings.background {
            
        case .light:
            
            unitContainerView.backgroundColor = UIColor.white
            unitTitleLabel.textColor = UIColor(named: .navy)
            unitTextField.textColor = UIColor.black
            
            amountContainerView.backgroundColor = UIColor.white
            amountTitleLabel.textColor = UIColor(named: .navy)
            amountTextField.textColor = .black
            
            typeContainerView.backgroundColor = UIColor.brown
            typeTitleLabel.textColor = .white
            
        case .dark:
            unitContainerView.backgroundColor = UIColor.black
            unitTitleLabel.textColor = UIColor.cyan
            unitTextField.textColor = .black
            
            amountContainerView.backgroundColor = UIColor.black
            amountTitleLabel.textColor = UIColor.cyan
            amountTextField.textColor = .black
            
            typeContainerView.backgroundColor = UIColor.brown
            typeTitleLabel.textColor = UIColor.white
        }
        
        let typeTap = UITapGestureRecognizer(target: self, action: #selector(self.typeHandler))
        typeContainerView.addGestureRecognizer(typeTap)
        pageControl.isUserInteractionEnabled = false
        pageControl.currentPage = 0
        typeTitleLabel.text = Type.getString(from: pageControl.currentPage)
        
        unitTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        amountTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    @objc func typeHandler() {
        var currentTypeIndex = self.pageControl.currentPage
        if currentTypeIndex < self.pageControl.numberOfPages - 1 {
            currentTypeIndex += 1
        } else {
            currentTypeIndex = 0
        }
        
        typeTitleLabel.text = Type.getString(from: currentTypeIndex)
        pageControl.currentPage = currentTypeIndex
        type = Type(rawValue: typeTitleLabel.text ?? "") ?? .market
        typeAction?()
    }
    
    public func configurationButtonAction(valueAction: ((_ enableConfirm: Bool) -> ())?,
                                          typeAction: (() -> ())?) {
        self.valueAction = valueAction
        self.typeAction = typeAction
    }
    
    public func update(
        unit: String, type: String) {
        
        //This cell can be reused, so title and page control should be updated based on inputs
    }
    
    public func getUnits() -> Int {
        guard let unitString = unitTextField.text, unitString.count > 0,
            let units = Int(unitString.replacingOccurrences(of: ",", with: "")), units > 0 else {
                return 0
        }
        return units
    }
    
    public func getAmount() -> Float {
        guard let amountString = amountTextField.text, amountString.count > 0,
            let amounts = floatFormatter.number(from: amountString.replacingOccurrences(of: ",", with: "")), amounts.floatValue > 0 else {
                return 0.0
        }
        return amounts.floatValue
    }
}

extension InputCell: UITextFieldDelegate {
    
    
    @objc func textFieldDidChange(textField: UITextField) {
        updateTextField(textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        updateTextField(textField)
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.layer.borderColor = UIColor.red.cgColor
        textField.layer.borderWidth = 2.0
        switch textField.tag {
        case UnitAmount.unit.rawValue:
            textField.text = intFormatter.number(from: textField.text ?? "")?.stringValue
        case UnitAmount.amount.rawValue:
            textField.text = floatFormatter.number(from: textField.text ?? "")?.stringValue
        default:
            return true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateTextField(textField)
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.resignFirstResponder()
    }
    
    public func updateTextField(_ textField: UITextField) {
        
        valueAction?(false)
        
        switch textField.tag {
        case UnitAmount.unit.rawValue:
            guard let unitString = unitTextField.text, unitString.count > 0, let units = Int(unitString.replacingOccurrences(of: ",", with: "")), units > 0 else {
                amountTextField.text = "NA"
                return
            }
            if price == -1.0 {
                amountTextField.text = "NA"
            } else {
                amountTextField.text = floatFormatter.string(from: NSNumber(value: Float(units) * price))
            }
            unitTextField.text = intFormatter.string(from: NSNumber(value: units))
            
        case UnitAmount.amount.rawValue:
            
            guard let amountString = amountTextField.text, amountString.count > 0, let amounts = floatFormatter.number(from: amountString.replacingOccurrences(of: ",", with: "") ), amounts.floatValue > 0 else {
                amountTextField.text = "NA"
                return
            }
            amountTextField.text = floatFormatter.string(from: amounts)
            if price == -1.0 {
                unitTextField.text = "NA"
            } else {
                unitTextField.text = intFormatter.string(from: NSNumber(value: Int(amounts.floatValue  / price)))
            }
            
        default:
            textField.resignFirstResponder()
        }
        
        guard let amountString = amountTextField.text, amountString.count > 0,
            let amounts = floatFormatter.number(from: amountTextField?.text ?? ""), amounts.floatValue > 0,
            let unitString = unitTextField.text, unitString.count > 0,
            let units = Int(unitString), units > 0 else {
                return
        }
        
        valueAction?(amounts.floatValue > 0 && units > 0)
    }
    
}

