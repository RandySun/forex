//
//  ViewController+TableView.swift
//  TicketOrder
//
//  Created by Randy Sun.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        tableView.contentInset.bottom = 20
        tableView.layer.masksToBounds = false
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorStyle = .none
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
        refreshControl.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return generateTableCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellType = self.viewModel.getTableCellType(indexPath: indexPath)
        if cellType == .estimatedMargin, let cell = tableView.cellForRow(at: indexPath) as? EstimationMarginCell {
            cell.onClick()
            cell.sizeToFit()
            cell.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    
    private func generateTableCell(indexPath: IndexPath) -> UITableViewCell {
        
        let cellType = self.viewModel.getTableCellType(indexPath: indexPath)
        
        switch cellType {
        case .inputCell:
            guard let ticket =  viewModel.displayedTicket else {
                return UITableViewCell()
            }
            let cell: InputCell = tableView.dequeueReusableCell(for: indexPath)
            cell.selectionStyle = .none
            guard let buyPriceString = ticket.getRateDetail(by: viewModel.currencyType)?.buyTT, let buyPrice = Float(buyPriceString), let sellPriceString = ticket.getRateDetail(by: viewModel.currencyType)?.sellTT, let sellPrice = Float(sellPriceString) else  {
                return cell
            }
            cell.price = viewModel.operationType == .buy ? buyPrice  : sellPrice
            cell.configurationButtonAction(valueAction: { (enabled) in
                self.confirmButton.isEnabled = enabled
            }) {
                //Add one more extra cell
            }
            return cell
        case .stopLoss:
            let cell: InformationCell = tableView.dequeueReusableCell(for: indexPath)
            cell.titleLabel.textColor = .red
            cell.titleLabel.text = viewModel.stopLossText
            cell.selectionStyle = .none
            return cell
            
        case .takeProfit:
            let cell: InformationCell = tableView.dequeueReusableCell(for: indexPath)
            cell.titleLabel.textColor = .green
            cell.titleLabel.text = viewModel.takeProfiltText
            cell.selectionStyle = .none
            return cell
            
        case .estimatedMargin:
            let cell: EstimationMarginCell = tableView.dequeueReusableCell(for: indexPath)
            cell.selectionStyle = .none
            let firstRow: GeneralDescriptionView = GeneralDescriptionView.fromNib()
            firstRow.update(title: viewModel.marginText1, subTitle: "0.0")
            let secondRow: GeneralDescriptionView = GeneralDescriptionView.fromNib()
            secondRow.update(title: viewModel.marginText2, subTitle: "0.0")
            cell.update(title: cell.titleLabel?.text ?? "", subTitle: cell.subTitleLabel?.text ?? "", subViews: [firstRow,secondRow])
            cell.sizeToFit()
            cell.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
            return cell
        
        case .stopEntryCell:
            let cell: StopEntryCell = tableView.dequeueReusableCell(for: indexPath)
            return cell
        }
        
    }
    
}
