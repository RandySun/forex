//
//  TickerViewModel.swift
//  TicketOrder
//
//  Created by Sun, Randy.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation

public enum OperationType {
    case buy,
    sell
}

public enum ViewModelState {
    case initial
    case loading
    case loaded
    case error(Error)
    case apiError(APIError)
}

public enum TicketCellIndex: Int {
    case inputCell = 0,
    stopLoss = 1,
    takeProfit = 2,
    estimatedMargin = 3,
    stopEntryCell = 4
}

protocol ViewModelDelegate: class {
    func update(for state: ViewModelState)
}

class TicketViewModel {
    weak var delegate: ViewModelDelegate? = nil
    var isLoading = false
    var ticket: CurrencyProduct? = nil
    
    var currencyType: Currency = .USD
    var marketType: Type = .market
    var displayedTicket: Product? = nil
    var operationType: OperationType = .buy
    
    //Todo, for stop entry, update this dict
    private let tableCell = [ 0: TicketCellIndex.inputCell,
                      1: TicketCellIndex.stopLoss,
                      2: TicketCellIndex.takeProfit
                      //3: TicketCellIndex.estimatedMargin
    ]
    
    //Todo: the following should go to localisation
    let buyText = "Buy"
    let sellText = "Sell"
    let cancelText = "Cancel"
    let confirmText = "Confirm"
    let stopLossText = "Stop loss"
    let takeProfiltText = "Take profit"
    let marginText1 = "Margin example 1"
    let marginText2 = "Margin example 2"
    
    init() {}
    
    func loadTicket(showAnimation: Bool = true) {
        
        guard !isLoading else { return }
        isLoading = true
        if showAnimation {
            DispatchQueue.main.async {
                self.delegate?.update(for: .loading)
            }
        }
        NetworkAPI.shared.getTicker {[weak self] response in
            guard let strongSelf = self else { return }
            strongSelf.isLoading = false
            switch response {
                
            case .success(let data):
                strongSelf.ticket = data.data.brands.wbc.portfolios.fx.products
                if let currency =  self?.currencyType, let product = strongSelf.ticket?.getTicket(by: currency) {
                    self?.displayedTicket = product
                } else {
                    self?.displayedTicket = strongSelf.ticket?.USD
                }
                DispatchQueue.main.async {
                    strongSelf.delegate?.update(for: .loaded)
                }
                return
            case .failure(let err):
                DispatchQueue.main.async {
                    strongSelf.delegate?.update(for: .apiError(err))
                }
                return
            }
        }
        
    }
    
    func getTableCellType(indexPath: IndexPath) -> TicketCellIndex {
        guard indexPath.row < self.tableCell.count else {
            return .inputCell
        }
        
        return tableCell[indexPath.row] ?? .inputCell
    }
    
    func getCellCount() -> Int {
        return tableCell.count
    }
    
}

