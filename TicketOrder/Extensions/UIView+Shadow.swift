//
//  UIView+Shadow.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 16/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UIView {
    static let defaultShadowRadius: CGFloat = 5.0

    public func addShadow(withRadius radius: CGFloat = 5.0) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = radius
        layer.shadowOpacity = 0.15
        layer.masksToBounds = false
    }
}

