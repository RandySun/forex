//
//  UITableViewCell+ReusableCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 9/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UITableViewCell {

    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }

    /// Note: Always name nibNames the same as their associated class, otherwise a crash will occur on dequeue.
    public static var nibName: String {
        return String(describing: self)
    }
}

protocol TableViewCellLoadsFromNib where Self: UITableViewCell {

}
