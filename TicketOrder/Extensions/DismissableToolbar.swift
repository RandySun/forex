//
//  DismissableToolbar.swift
//  TeachersHealthiOS
//
//  Created by Burney, Moe (AU - Sydney) on 23/8/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit


class DismissableToolbar: UIToolbar {
    
    convenience init(with dismissButtonItemType: UIBarButtonItem.SystemItem) {
        self.init(frame: CGRect.zero)
        setButtonItems(dismissButtonItemType: dismissButtonItemType)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        self.barStyle = .default
        setButtonItems(dismissButtonItemType: .cancel)
    }
    
    func setButtonItems(dismissButtonItemType: UIBarButtonItem.SystemItem) {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: dismissButtonItemType, target: self, action: #selector(self.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        self.setItems([spaceButton, cancelButton], animated: true)
        self.isUserInteractionEnabled = true
        self.sizeToFit()
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func cancelPressed() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
