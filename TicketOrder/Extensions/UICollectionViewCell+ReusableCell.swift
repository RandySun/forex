//
//  UICollectionViewCell+ReusableCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 29/8/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

/* A collection view cell conforming to this protocol will use the
 defaultReuseIdentifier and nibName as defined in the extension on
 UICollectionReusableView on dequeue.

 However, UICollectionViewCells need to conform to this protocol rather
 than CollectionReusableViewLoadsFromNib to properly load the nib.*/
protocol CollectionViewCellLoadsFromNib where Self: UICollectionViewCell {

}
