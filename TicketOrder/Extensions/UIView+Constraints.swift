//
//  UIView+Stack.swift
//  TeachersHealthiOS
//
//  Created by Sun, Randy on 2/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UIView {
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    
    func stackViewsVerticallyWithHorizontalPadding(_ views: [UIView], verticalPadding: CGFloat = 0, horizontalPadding: CGFloat) {
        var topView = self
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(view)
            
            let top    = (topView == self) ? "|" : "[topView]-(padding@750)-"
            let bottom = view == views.last ? "-(0@1000)-|" : ""
            
            // Vertical
            let verticalConstraints = NSLayoutConstraint.constraints(
                withVisualFormat: "V:\(top)[view]\(bottom)",
                options: NSLayoutConstraint.FormatOptions(),
                metrics: ["padding": verticalPadding],
                views: ["view": view, "topView": topView])
            addConstraints(verticalConstraints)
            
            // Horizontal
            self.addConstraints(
                NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-(\(horizontalPadding)@1000)-[view]-(\(horizontalPadding)@1000)-|",
                    options: NSLayoutConstraint.FormatOptions(),
                    metrics: nil,
                    views: ["view": view]))
            
            topView = view
        }
    }
    
    func stackViewsHorizontally(_ views: [UIView], span: CGFloat = 0) {
        var leadingView = self
        var leading: String
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
            let trailing = (view == views.last) ? "-0@1000-|" : ""
            
            if leadingView == self {
                leading = "|"
                addConstraints(
                    NSLayoutConstraint.constraints(
                        withVisualFormat: "H:\(leading)[view]\(trailing)",
                        options: NSLayoutConstraint.FormatOptions(),
                        metrics: nil,
                        views: ["view": view, "leadingView": leadingView]))
            } else {
                leading = "[leadingView]-(\(span)@1000)-"
                addConstraints(
                    NSLayoutConstraint.constraints(
                        withVisualFormat: "H:\(leading)[view]\(trailing)",
                        options: NSLayoutConstraint.FormatOptions(),
                        metrics: nil,
                        views: ["view": view, "leadingView": leadingView]))
            }
            
            // Vertical constraints
            addConstraints(
                NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|[view]|",
                    options: NSLayoutConstraint.FormatOptions(),
                    metrics: nil,
                    views: ["view": view]))
            leadingView = view
        }
    }
}

