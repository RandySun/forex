//
//  UIView+Nib.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 18/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

/**
 An extension on UView that contains functions to easily load nibs.
 */
extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self),
                                        owner: nil,
                                        options: nil)![0] as! T
    }
}
