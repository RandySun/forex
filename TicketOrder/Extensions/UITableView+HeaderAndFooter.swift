//
//  UITableView+HeaderAndFooter.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 12/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UITableView {

    /// Set table header view and add layout constraints.
    func setTableHeaderView(headerView: UIView) {
        headerView.translatesAutoresizingMaskIntoConstraints = false

        // Set table header view.
        self.tableHeaderView = headerView

        // Set constraints.
        headerView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }

    /// Update header view's frame.
    func updateHeaderViewFrame() {
        guard let headerView = self.tableHeaderView else { return }

        // Update the size of the header based on its internal content.
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()

        let height = headerView.systemLayoutSizeFitting(
            UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame

        // Trigger table view to know that header should be updated.
        self.tableHeaderView = headerView
    }

     /// Update footer view's frame.
    func updateFooterViewFrame() {
        guard let footerView = self.tableFooterView else { return }

        // Update the size of the header based on its internal content.
        footerView.setNeedsLayout()
        footerView.layoutIfNeeded()

        let height = footerView.systemLayoutSizeFitting(
            UIView.layoutFittingCompressedSize).height

        var frame = footerView.frame
        frame.size.height = height
        footerView.frame = frame

        // Trigger table view to know that footer should be updated.
        let footer = self.tableFooterView
        self.tableFooterView = footer
    }
}
