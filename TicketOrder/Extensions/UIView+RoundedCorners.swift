//
//  UIView+RoundedCorners.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 9/9/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UIView {
    func addRoundedCorners(with radius: CGFloat = 5.0) {
        self.layer.cornerRadius = radius
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }
}
