//
//  PhoneNumber.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 29/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

// Helper class for phone number related methods.
public class PhoneNumber {
    static func call(_ number: String) {
        // Strip phone number string to only take numeric characters.
        let formattedNumber = number.components(
            separatedBy: CharacterSet.decimalDigits.inverted).joined()

        // Then attempt to open formatted number as URL.
        if let url = URL(string: "tel://\(formattedNumber)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
