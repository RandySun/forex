//
//  UICollectionReusableView+ReusableCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 30/8/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UICollectionReusableView {
    public static var defaultReuseIdentifier: String {
        return String(describing: self)
    }

    public static var nibName: String {
        return String(describing: self)
    }
}

protocol CollectionReusableViewLoadsFromNib where Self: UICollectionReusableView {

}
