//
//  NavigationViewController+Extensions.swift
//  TeachersHealthiOS
//
//  Created by Sun, Randy on 26/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    //A better way to get this title label should extend NavigationController, but since it needs to change coordinator and others, so we will have it here till we have time to refactor it
    public func findTitleLabelFromNaviBar(value: String) -> UILabel? {
        for subview in self.navigationBar.subviews {
            if let findClass = NSClassFromString("_UINavigationBarContentView"), subview.isKind(of: findClass)  {
                for newsubviews in subview.subviews {
                    if let label: UILabel = newsubviews as? UILabel, let text = label.text, text == value.uppercased() {
                        return label
                    }
                }
                return nil
            }
        }
        return nil
    }
    
}

