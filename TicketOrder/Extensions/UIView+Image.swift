//
//  UIView+Image.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 15/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UIView {
    func convertToImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
