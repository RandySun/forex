//
//  UILabel+LineHeight.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 22/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

extension UILabel {
    func setText(to text: String, withLineHeightMultiple lineHeightMultiple: CGFloat) {
        let attributedString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        attributedText = attributedString
    }
}
