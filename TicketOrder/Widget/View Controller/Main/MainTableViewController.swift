//
//  MainTableViewController.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 17/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class MainTableViewController: MainViewController {
    @IBOutlet weak var tableView: UITableView!

    convenience init() {
        self.init(nibName: String(describing: MainTableViewController.self), bundle: nil)
    }
}
