//
//  MainViewController.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 17/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit
//import Lottie

class MainViewController: UIViewController {
    
    // Sometimes the dismiss analytics event should be called rather than "go back" event
    // (Even if the VC is just being popped from the nav stack)
    // This bool can be used by any superclass to allow that.
    var isDismissedForAnalytics: Bool = false
    
    private var loadingView: UIView?
    private var container: UIView?
    
    private var loadingIndicatorSize: (width: CGFloat, height: CGFloat, padding: CGFloat) = (width: 60, height: 60, padding: 10)

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if loadingView == nil {
            setupLoadingIndicator(forView: view)
        }
        
        // Hide default back button text by making it an empty string 
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
    }

    /// Show/hide loading view.
    public func showLoading(_ visible: Bool, disableNaviBar: Bool = false, onComplete: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3,
                           animations: {
                            self.loadingView?.alpha = visible ? 1.0 : 0.0
            }) { [weak self] completion in
                self?.loadingView?.isHidden = !visible
                // ENHANCEMENT: consider how to disable/enable tabbar as well, don't want to add a blocking view in this way: UIApplication.shared.keyWindow!.addSubview(loadingView)
//                if let navigationController = self?.navigationController as? NavigationController {
//                    navigationController.enableNaviBar(enabled: !visible)
//                }
//                if visible {
//                    if onComplete != nil { self?.loadingAnimation?.loopMode = .playOnce }
//                    self?.loadingAnimation?.play(completion: { _ in
//                        if let onComplete = onComplete {
//                            onComplete()
//                        }
//                    })
//                } else {
//                    self?.loadingAnimation?.stop()
//                }
            }
        }
    }
    
    typealias ExternalLinkAnalytics = (selected: String?, leave: String?, cancelled: String?)

    /// Handle open external link action.
    public func openExternalLink(_ link: String,
                                analytics: ExternalLinkAnalytics = ExternalLinkAnalytics(
                                                                        selected: nil,
                                                                        leave: nil,
                                                                        cancelled: nil)) {
            
        // Open external link, only if the given link is a valid URL and can be opened.
        guard
            let url = URL(string: link),
            UIApplication.shared.canOpenURL(url) else { return }
        let externalLinkAlert = Alert(
            ofType: .action,
            title: String.localizedStringWithFormat(
                NSLocalizedString("alert_external_app_title", comment: ""),
                NSLocalizedString("app_name",
                                  tableName: "target_name",
                                  comment: "")),
            message: String.localizedStringWithFormat(
                NSLocalizedString("alert_external_app_message", comment: ""),
                NSLocalizedString("app_short",
                                  tableName: "target_name",
                                  comment: "")),
            actions: [(NSLocalizedString("alert_external_app_action", comment: ""),
                       { _ in UIApplication.shared.open(url)
            })],
            cancelActions: [(NSLocalizedString("general_cancel", comment: ""),
                             { _ in
                                
            })]
        )
        self.present(Alert.createPopup(from: externalLinkAlert), animated: true)

        
    }

    /// Present an error (alert controller) given an error type and optional actions.
//    public func showError(
//        error: APIError?,
//        reload: ((UIAlertAction) -> Void)? = nil) {
//
//        guard let _ = error else {
//            return
//        }
//
//        switch error {
//        case .networkError:
//            showNetworkErrorAlert(reload: reload)
//        case .badRequest(let errorModel):
//            showBadRequestAlert(from: errorModel)
//        case .unauthorized(_):
//            showUnauthorizedAlert()
//        case .serverError, .internalError:
//            showServerErrorAlert()
//        case .tooManyRequests:
//            showTooManyRequestsAlert(reload: reload)
//        default:
//            showGenericErrorAlert()
//        }
//    }
}

// MARK: Private loading indicator helper functions.
extension MainViewController {
    // Set up loading indicator in view.
    private func setupLoadingIndicator(forView view: UIView) {
        // Loading view
        loadingView = UIView(frame: view.safeAreaLayoutGuide.layoutFrame)
        guard let loadingView = loadingView else { return }
        view.addSubview(loadingView)
        view.bringSubviewToFront(loadingView)
        loadingView.backgroundColor = UIColor(named: .backgroundLight)
        
        setupLoadingIndicator()

        // Hide loading view.
        loadingView.isHidden = true
    }
    
    public func setupLoadingIndicator(loadingIndicatior: String = Constants.Animations.loading) {
        // Loading animation container view
        if let loadingView = loadingView {
            let containerOrigin = CGPoint(x: loadingView.frame.width / 2 - CGFloat(loadingIndicatorSize.width) / 2 - CGFloat(loadingIndicatorSize.padding),
                                          y: loadingView.frame.height / 2 - CGFloat(loadingIndicatorSize.height) / 2 - CGFloat(loadingIndicatorSize.padding))
            
            container?.removeFromSuperview()
            
            container = UIView(frame: CGRect(x: containerOrigin.x,
                                             y: containerOrigin.y,
                                             width: CGFloat(loadingIndicatorSize.width + loadingIndicatorSize.padding * 2),
                                             height: CGFloat(loadingIndicatorSize.height + loadingIndicatorSize.padding * 2)))
            
            if let container = container {
                loadingView.addSubview(container)
                
                // Loading animation
//                if let _ = NSDataAsset(name: loadingIndicatior, bundle: Bundle.main) {
//                    do {
//                        //
//                    }
//                    catch {
//                        // For now, don't handle error.
//                    }
//                }
            }
        }
    }
}

// MARK: Private error handling helper functions.
// REFACTOR: these "show error alert" functions should be moved to their own coordinator
extension MainViewController {
    // Show alert on network/connection error.
    private func showNetworkErrorAlert(reload: ((UIAlertAction) -> Void)?) {
        let networkErrorAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("network_error_title", comment: ""),
            message: NSLocalizedString("network_error_message", comment: ""),
            actions: reload != nil
                ? [(title: NSLocalizedString("general_try_again", comment: ""), handler: reload!)]
                : nil,
            dismissButtonTitle: NSLocalizedString("general_ok_got_it", comment: ""))

        present(Alert.createPopup(from: networkErrorAlert), animated: true)
    }

    // Show alert on bad request error.
    private func showBadRequestAlert(from error: APIErrorModel?) {
        let badRequestAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("general_error_title", comment: ""),
            message: error?.message ?? NSLocalizedString("general_error_message", comment: ""),
            dismissButtonTitle: NSLocalizedString("general_ok_got_it", comment: ""))

        present(Alert.createPopup(from: badRequestAlert), animated: true)
    }

    // Show alert on authorization error.
    private func showUnauthorizedAlert() {
        let unauthorizedAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("logout_timeout_title", comment: ""),
            message: NSLocalizedString("logout_timeout_message", comment: ""),
            actions: [
                (title: NSLocalizedString("login_sign_in_button", comment: ""),
                 handler: { (_: UIAlertAction) in
                    //let _ = AppManager.shared.authentication.logout().subscribe()
                })])

        present(Alert.createPopup(from: unauthorizedAlert), animated: true)
    }
    
    // Show alert on server error.
    private func showServerErrorAlert() { // should reload too i guess
        let serverErrorAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("server_error_title", comment: ""),
            message: NSLocalizedString("server_error_message", comment: ""),
            actions: [
                (title: NSLocalizedString("server_error_negative_button", comment: ""),
                 handler: { (_: UIAlertAction) in
                    //let _ = AppManager.shared.authentication.logout().subscribe()
                }),
                (title: NSLocalizedString("server_error_positive_button", comment: ""),
                 handler: { _ in
                    MainViewController.goToWeb()
                })])

        present(Alert.createPopup(from: serverErrorAlert), animated: true)
    }

    // Show alert on too many requests.
    private func showTooManyRequestsAlert(reload: ((UIAlertAction) -> Void)?) {
        let tooManyRequestsAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("general_error_title", comment: ""),
            message: NSLocalizedString("general_error_message", comment: ""),
            actions: reload != nil
                ? [(title: NSLocalizedString("general_try_again", comment: ""), handler: reload!)]
                : nil,
            dismissButtonTitle: NSLocalizedString("general_ok", comment: ""))

        present(Alert.createPopup(from: tooManyRequestsAlert), animated: true)
    }

    // Show generic error alert.
    private func showGenericErrorAlert() {
        let genericAlert = Alert(
            ofType: .error,
            title: NSLocalizedString("general_error_title", comment: ""),
            message: NSLocalizedString("general_error_message", comment: ""),
            dismissButtonTitle: NSLocalizedString("general_ok", comment: ""))

        present(Alert.createPopup(from: genericAlert), animated: true)
    }

    // Handle 'go to web' action on server error.
    // REFACTOR: move somewhere else
    private static func goToWeb() {
        let link = NSLocalizedString("url_member_services", tableName: "target_name", comment: "")
        guard let url = URL(string: link),
            UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
}
