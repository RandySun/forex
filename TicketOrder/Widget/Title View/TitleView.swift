//
//  TitleView
//  TeachersHealthiOS
//
//  Created by Sun, Randy on 12/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit

class TitleView: UIView {
    @IBOutlet weak var titleLabel: Heading2!
    @IBOutlet weak var leadingContraints: NSLayoutConstraint!
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor(named: .backgroundLight)
        titleLabel.textColor = UIColor(named: .navy)
    }
}
