//
//  WebViewController.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 20/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit
import WebKit

final class WebViewController: MainViewController {

    var webView: WebView = WebView()
    @IBOutlet weak var webViewTabBar: UITabBar!

    let progressView: UIProgressView = {
        let view = UIProgressView(progressViewStyle: .default)
        view.progressTintColor = UIColor(named: .primaryDark)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    deinit {
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()

        webView.addObserver(self,
                            forKeyPath: #keyPath(WKWebView.estimatedProgress),
                            options: .new,
                            context: nil)

        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
    }

    func setupUI() {
        view.backgroundColor = UIColor.white
        view.insertSubview(webView, belowSubview: webViewTabBar)
        webView.frame = .zero

        // Progress bar
        view.addSubview(progressView)
        progressView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        progressView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        progressView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        progressView.heightAnchor.constraint(equalToConstant: 4).isActive = true

        // Tab bar
        webViewTabBar.delegate = self
        webViewTabBar.unselectedItemTintColor = webViewTabBar.tintColor
        webView.bottomAnchor.constraint(equalTo: webViewTabBar.topAnchor).isActive = true

        let tabBarItems: [TabItemType] = [.back, .forward, .refresh]
        for item in tabBarItems {
            addTabItem(buttonType: item)
        }
        self.updateTabBarButtonLayout()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let guide = view.safeAreaLayoutGuide
        let height = guide.layoutFrame.size.height

        webView.frame = CGRect(x: 0,
                               y: view.safeAreaInsets.top,
                               width: view.frame.width,
                               height: height - webViewTabBar.frame.height)
    }

    func loadWebView(urlString: String) {
        webView.loadWebView(urlString: urlString)
    }

    /// Methods to display loading status.
    func setLoadingStatus(visible: Bool) {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.progressView.alpha = visible ? 1 : 0
            }, completion: nil)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
}

extension WebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        setLoadingStatus(visible: true)
        updateTabBarButtonLayout()
    }

    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        setLoadingStatus(visible: false)
        updateTabBarButtonLayout()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        setLoadingStatus(visible: false)
        updateTabBarButtonLayout()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        setLoadingStatus(visible: false)
        updateTabBarButtonLayout()
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        setLoadingStatus(visible: false)
        updateTabBarButtonLayout()
    }
}

extension WebViewController: UITabBarDelegate {
    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let tabBarType = TabItemType(rawValue: item.tag),
            let _ = webView.url else { return }

        switch tabBarType {
        case .back:
            if webView.canGoBack {
                webView.goBack()
            }
        case .forward:
            if webView.canGoForward {
                webView.goForward()
            }

        case .refresh:
            webView.reload()
        }

        self.updateTabBarButtonLayout()
    }

    /// An enumeration of web view tab bar items.
    public enum TabItemType: Int {
        case back
        case forward
        case refresh
    }

    /// An enumeration declaring web view tab bar items.
    public enum TabItem: Int {
        case back
        case forward
        case refresh

        var image: UIImage {
            switch self {
            case .back: return UIImage(named: .webViewBack)
            case .forward: return UIImage(named: .webViewForward)
            case .refresh: return UIImage(named: .webViewRefresh)
            }
        }

        var accessibilityLabel: String {
            switch self {
            case .back: return NSLocalizedString("general_back", comment: "")
            case .forward: return NSLocalizedString("general_forward", comment: "")
            case .refresh: return NSLocalizedString("general_refresh", comment: "")
            }
        }
    }

    /// Add tab item to tab bar.
    private func addTabItem(buttonType: TabItemType) {
        guard let tabItem = TabItem(rawValue: buttonType.rawValue) else { return }
        let newItem = UITabBarItem(title: nil, image: tabItem.image, tag: buttonType.rawValue)
        newItem.accessibilityLabel = tabItem.accessibilityLabel
        self.webViewTabBar.items?.append(newItem)
    }

    /// Updates the look of buttons after tap (e.g. selected or not)
    internal func updateTabBarButtonLayout() {
        guard let tabBarItems = webViewTabBar.items else { return }
        for item in tabBarItems {
            guard let type = TabItemType(rawValue: item.tag) else { return }

            switch type {
            case .back:
                item.isEnabled = webView.canGoBack
            case .forward:
                item.isEnabled = webView.canGoForward
            case .refresh:
                break
            }
        }
    }
}
