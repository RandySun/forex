//
//  WebView.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 20/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit
import WebKit

final class WebView: WKWebView {
    var viewModel: WebViewModel?

    func loadWebView(urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}

/** Implementation of WebKit navigation delegate methods.
 */
extension WebView: WKNavigationDelegate {

    /// Implement navigation action policy for whitelisting & phone number/e-mail links.
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let request = navigationAction.request

        // Handle permitted/blocked web links.
        if let permitted = viewModel?.permitNavigation(to: request), permitted {
            decisionHandler(.allow)
            return
        }

        decisionHandler(.cancel)
    }

    /// Implement navigation response policy based on authorization requirements.
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {


        decisionHandler(.allow)
    }
}

