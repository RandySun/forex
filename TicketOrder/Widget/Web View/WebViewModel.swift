//
//  WebViewModel.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 20/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation

final class WebViewModel {

    var whitelist: [String]?

    init() {
        self.whitelist = [String]()
    }

    /// Implement whitelisting functions.
    func permitNavigation(to request: URLRequest) -> Bool {
        guard let url = request.url else { return false }

        return navigationAllowed(for: url)
    }

    private func navigationAllowed(for url: URL) -> Bool {
        guard let whitelist = whitelist, let host = url.host else { return false }

        for url in whitelist {
            // Only allow exact host match or any subdomain of allowed domain.
            if host == url || host.contains("." + url) {
                return true
            }
        }

        return false
    }
}
