import UIKit

class Body: UILabel {
    
    func setup() {
        font = ScaledFont.shared.font(forTextStyle: .body)
        adjustsFontForContentSizeCategory = true
    }
    
    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}
