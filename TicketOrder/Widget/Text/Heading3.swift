import UIKit

class Heading3: UILabel {
    
    func setup() {
        // GrotaSansAltRd-Bold 20pt
        font = ScaledFont.shared.font(forTextStyle: .title2)
        adjustsFontForContentSizeCategory = true
    }
    
    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

}
