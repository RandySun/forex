import UIKit

class Label1: UILabel {
    
    func setup() {
        // OpenSans-Bold 12pt
        font = ScaledFont.shared.font(forTextStyle: .subheadline)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

}
