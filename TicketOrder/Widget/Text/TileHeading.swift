import UIKit

class TileHeading: UILabel {
    
    func setup() {
        font = ScaledFont.shared.font(forTextStyle: .caption2)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}
