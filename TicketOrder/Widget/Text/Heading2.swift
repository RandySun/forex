import UIKit

class Heading2: UILabel {
    
    func setup() {
        // GrotaSansAltRd-Bold 32pt
        font = ScaledFont.shared.font(forTextStyle: .title1)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}
