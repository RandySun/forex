//
//  Callout.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 15/8/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class Label2: UILabel {

    func setup() {
        // OpenSans-Bold 16pt
        font = ScaledFont.shared.font(forTextStyle: .callout)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}
