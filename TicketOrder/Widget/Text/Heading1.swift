import UIKit

class Heading1: UILabel {
    
    func setup() {
        // GrotaSansAltRd-Bold 40pt
        font = ScaledFont.shared.font(forTextStyle: .largeTitle)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}
