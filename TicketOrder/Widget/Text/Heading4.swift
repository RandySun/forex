import UIKit

class Heading4: UILabel {
    
    func setup() {
        // OpenSans-Bold 20pt
        font = ScaledFont.shared.font(forTextStyle: .title3)
        adjustsFontForContentSizeCategory = true
    }

    /// Define custom initializers.
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

}
