//
//  HomeNavigationTileTableViewCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 5/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class HomeNavigationTileTableViewCell: UITableViewCell, TableViewCellLoadsFromNib {
    @IBOutlet weak var topDivider: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var actionImageView: UIImageView!
    @IBOutlet weak var titleLabel: TileHeading!

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        backgroundColor = UIColor(named: .backgroundLight)
        titleLabel?.textColor = UIColor(named: .navy)
    }

    public func update(title: String, imageName: String?) {
        titleLabel.text = title
        if let imageName = imageName {
            iconImageView.image = UIImage(named: imageName)
        }

        actionImageView.image = UIImage(named: .tableViewCellRoute).withRenderingMode(.alwaysTemplate)
        actionImageView.tintColor = UIColor(named: .primaryDark)
    }
}
