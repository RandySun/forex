//
//  TableViewCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 2/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell, TableViewCellLoadsFromNib {
    @IBOutlet weak var titleLabel: UILabel!
    var textStyle: UIFont.TextStyle = .caption2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: textStyle)
        titleLabel?.adjustsFontForContentSizeCategory = true
        backgroundColor = UIColor(named: .backgroundLight)
        self.layoutMargins = .zero
        self.separatorInset = .zero
        self.contentView.layoutMargins.right = 0
        if let accessoryView = self.accessoryView {
            accessoryView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint(item: accessoryView as Any,
                               attribute: .trailing,
                               relatedBy: .equal,
                               toItem: self,
                               attribute: .trailing,
                               multiplier: 1,
                               constant: -16).isActive = true
            NSLayoutConstraint(item: accessoryView as Any,
                               attribute: .centerY,
                               relatedBy: .equal,
                               toItem: self,
                               attribute: .centerY,
                               multiplier: 1,
                               constant: 0).isActive = true
        }
    }
}
