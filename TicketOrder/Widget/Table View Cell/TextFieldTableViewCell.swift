//
//  TextFieldTableViewCell.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 30/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell, TableViewCellLoadsFromNib {
    @IBOutlet weak var titleLabel: Label1!
    @IBOutlet weak var textField: TextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        backgroundColor = UIColor(named: .backgroundLight)
        titleLabel?.textColor = UIColor(named: .navy)
    }
}
