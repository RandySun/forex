//
//  InformationTableViewCell.swift
//  TeachersHealthiOS
//
//  Created by Sun, Randy on 12/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit

class InformationTableViewCell: UITableViewCell, TableViewCellLoadsFromNib {
    @IBOutlet weak var titleLabel: Label1!
    @IBOutlet weak var detailLabel: Body!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor(named: .backgroundLight)
        titleLabel?.textColor = UIColor(named: .navy)
        detailLabel?.textColor = UIColor(named: .navy)
    }
    
    public func update(title: String, details: String) {
        titleLabel.text = title.localizedUppercase
        detailLabel.text = details
    }
}
