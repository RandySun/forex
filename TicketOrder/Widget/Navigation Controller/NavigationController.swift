//
//  NavigationController.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 17/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setup() {
        navigationBar.titleTextAttributes = [.font: ScaledFont.shared.font(forTextStyle: .caption1),
                                             .foregroundColor: UIColor.white]
        navigationBar.tintColor = .white
        navigationBar.isTranslucent = false
        
        // Back arrow
        let backButtonImage = UIImage(named: .backButton)
        navigationBar.backIndicatorImage = backButtonImage
        navigationBar.backIndicatorTransitionMaskImage = backButtonImage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func enableNaviBar( enabled: Bool ) {
        self.navigationBar.isUserInteractionEnabled = enabled
    }
}
