

import UIKit

class BannerView: UIView {

    private let label = Label1()

    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor(named: .updateBG)

        self.addSubview(self.label)

        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.textColor = UIColor(named: .updateText)
        self.label.numberOfLines = 0
        self.label.textAlignment = .center

        // success label constraints
        NSLayoutConstraint(
            item: self.label,
            attribute: .top,
            relatedBy: .equal,
            toItem: self,
            attribute: .top,
            multiplier: 1.0,
            constant: 8.0).isActive = true

        NSLayoutConstraint(
            item: self.label,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self,
            attribute: .bottom,
            multiplier: 1.0,
            constant: -8.0).isActive = true

        NSLayoutConstraint(
            item: self.label,
            attribute: .leading,
            relatedBy: .equal,
            toItem: self,
            attribute: .leading,
            multiplier: 1.0,
            constant: 15.0).isActive = true

        NSLayoutConstraint(
            item: self.label,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: self,
            attribute: .trailing,
            multiplier: 1.0,
            constant: -15.0).isActive = true
    }

    func addToView(view: UIView) {
        view.removeFromSuperview()
        view.addSubview(self)

        NSLayoutConstraint(
            item: self,
            attribute: .top,
            relatedBy: .equal,
            toItem: view,
            attribute: .top,
            multiplier: 1.0,
            constant: 0.0).isActive = true

        NSLayoutConstraint(
            item: self,
            attribute: .leading,
            relatedBy: .equal,
            toItem: view,
            attribute: .leading,
            multiplier: 1.0,
            constant: 0.0).isActive = true

        NSLayoutConstraint(
            item: self,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: view,
            attribute: .trailing,
            multiplier: 1.0,
            constant: 0.0).isActive = true
    }

    func updateLabel(text: String) {
        self.label.text = text
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

}
