//
//  ValidatableTextFieldView.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 6/8/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class ValidatableTextFieldView: UIView {
    @IBOutlet weak var titleLabel: Label1!
    @IBOutlet weak var textField: TextField!
    @IBOutlet weak var validationLabel: Label1!
}
