import Foundation
import UIKit

class TextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    let padding = UIEdgeInsets.init(top: 12, left: 12, bottom: 12, right: 12)
    
    private func setup() {
        font = ScaledFont.shared.font(forTextStyle: .body)
        textColor = UIColor(named: .textFieldText)
        addRoundedCorners()
        layer.borderColor = UIColor(named: .textFieldBorder).cgColor
        layer.borderWidth = 1.0
        adjustsFontForContentSizeCategory = true
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
