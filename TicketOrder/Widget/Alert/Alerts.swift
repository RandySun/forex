//
//  Error.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 25/6/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit

struct Alert {
    enum AlertType {
        case information
        case action
        case error
    }

    // Define an alert action as a tuple taking in a title and a handler.
    // REFACTOR: use rx stop passing around closures containing onNext() stuff
    typealias AlertAction = (title: String, handler: ((UIAlertAction) -> Void))

    let title: String?
    let message: String
    let type: Alert.AlertType
    let actions: [AlertAction]?
    let dismissButtonTitle: String?
    let cancelActions:[AlertAction]?

    init(
        ofType type: Alert.AlertType,
        title: String? = nil,
        message: String,
        actions: [AlertAction]? = nil,
        dismissButtonTitle: String? = nil,
        cancelActions: [AlertAction]? = nil) {

        self.type = type
        self.title = title
        self.message = message
        self.actions = actions
        self.dismissButtonTitle = dismissButtonTitle
        self.cancelActions = cancelActions
    }

    // Returns a popup (alert controller) given an alert object.
    static func createPopup(from alert: Alert) -> UIAlertController {
        let controller = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)

        // Handle different alert types.
        switch alert.type {
        case .information:
            break
        case .action, .error:
            // Add actions if available.
            if let actions = alert.actions {
                for action in actions {
                    controller.addAction(UIAlertAction(title: action.title, style: .default, handler: action.handler))
                }
            }
            // Dismiss button actions if available.
            if let cancelActions = alert.cancelActions {
                for cancelAction in cancelActions {
                    controller.addAction(UIAlertAction(title: cancelAction.title, style: .cancel, handler: cancelAction.handler))
                }
            }
        }

        // Add dismiss button if available.
        if let dismissButtonTitle = alert.dismissButtonTitle {
            controller.addAction(UIAlertAction(title: dismissButtonTitle, style: .cancel, handler: nil))
        }

        return controller
    }

    // Returns an external link popup (alert controller) given a URL.
    static func createExternalLinkPopup(_ url: URL) -> UIAlertController {
        let externalLinkAlert = Alert(
            ofType: .action,
            title: String.localizedStringWithFormat(
                NSLocalizedString("alert_external_app_title", comment: ""),
                NSLocalizedString("app_name",
                                  tableName: "target_name",
                                  comment: "")),
            message: String.localizedStringWithFormat(
                NSLocalizedString("alert_external_app_message", comment: ""),
                NSLocalizedString("app_short",
                                  tableName: "target_name",
                                  comment: "")),
            actions: [(NSLocalizedString("alert_external_app_action", comment: ""),
                { _ in UIApplication.shared.open(url)})],
            cancelActions: [(NSLocalizedString("general_cancel", comment: ""),
                { _ in })]
        )
        return Alert.createPopup(from: externalLinkAlert)
    }
}
