//
//  TertiaryLinkWithImage.swift
//  TeachersHealthiOS
//
//  Created by Sun, Randy on 23/9/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class TertiaryLinkWithImage: UIView  {
    
    enum ImageAlignment {
        case left
        case right
    }
    
    private var contentView:UIView?
    private let nibName: String = "TertiaryLinkWithImage"
    private var buttonTextFont = ScaledFont.shared.font(forTextStyle: .callout)
    var rightTextLabel: UILabel?
    var rightImageView: UIImageView?
    var leftTextLabel: UILabel?
    var leftImageView: UIImageView?
    var actionButton: UIButton?
    var action: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nibSetup()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        nibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    func setTitle(title: String,
                  action: (() -> ())?,
                  image: UIImage? = nil,
                  imageAlignment: ImageAlignment = .right,
                  buttonTextFont: UIFont =  ScaledFont.shared.font(forTextStyle: .callout)) {
        
        rightTextLabel?.text = imageAlignment == .right ? title : ""
        rightTextLabel?.font = buttonTextFont
        leftTextLabel?.font = buttonTextFont
        leftTextLabel?.text = imageAlignment == .left ? title : ""
        leftTextLabel?.isHidden = imageAlignment == .right ? true : false
        leftImageView?.isHidden = imageAlignment == .right ? true : false
        rightTextLabel?.isHidden = imageAlignment == .left ? true : false
        rightImageView?.isHidden = imageAlignment == .left ? true : false
        if imageAlignment == .left {
            leftImageView?.image = image
        } else {
            rightImageView?.image = image
        }
        self.action = action
        sizeToFit()
    }
    
    private func setup() {
        rightTextLabel?.font = self.buttonTextFont
        rightTextLabel?.textColor = UIColor(named: .primaryDark)
        leftTextLabel?.textColor = UIColor(named: .primaryDark)
        rightTextLabel?.numberOfLines = 0
        leftTextLabel?.numberOfLines = 0
        actionButton?.addTarget(self, action: #selector(self.buttonAction), for: [.touchUpInside])
    }
    
    private func nibSetup() {
        guard let view = loadViewFromNib() else { return }
        self.backgroundColor = .clear
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        for view in contentView?.subviews ?? [] {
            
            if let button = view as? UIButton {
                actionButton = button
            }
            
            //Read image right side and label
            if let label = view as? UILabel {
                rightTextLabel = label
            }
            if let imageView = view as? UIImageView {
                self.rightImageView = imageView
            }
            
            //Read image left side and label
            if view.subviews.count > 0 {
                for subview in view.subviews {
                    if let label = subview as? UILabel {
                        leftTextLabel = label
                    }
                    if let imageView = subview as? UIImageView {
                        self.leftImageView = imageView
                    }
                }
            }
        }
    }
    
    @objc func buttonAction() {
        action?()
    }

    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
}
