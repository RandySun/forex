import UIKit

class PrimaryButton: UIButton {
    private func setup() {
        titleLabel?.textColor = UIColor.white
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: .callout)
        titleLabel?.adjustsFontForContentSizeCategory = true
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byWordWrapping
        addRoundedCorners()
        clipsToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        titleLabel?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true

        // Set initial background color based on enabled state.
        backgroundColor = isEnabled ? UIColor(named: .primaryDark) : UIColor(named: .disabled)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }

    // Change background color when button changes enabled state.
    override var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? UIColor(named: .primaryDark) : UIColor(named: .disabled)
        }
    }
}
