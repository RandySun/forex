//
//  TertiaryLinkLight.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 9/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class TertiaryLinkLight: UIButton {

    private func setup() {
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: .callout)
        titleLabel?.adjustsFontForContentSizeCategory = true
        titleLabel?.textColor = UIColor(named: .accentLight)
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byWordWrapping
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
}

extension TertiaryLinkLight {
    func addImage(_ image: UIImage) {
        let imageTemplate = image.withRenderingMode(.alwaysTemplate)
        setImage(imageTemplate, for: .normal)
        tintColor = UIColor(named: .accentLight)
        contentEdgeInsets = UIEdgeInsets(top: 0,
                                         left: 10,
                                         bottom: 0,
                                         right: 0)
        titleEdgeInsets = UIEdgeInsets(top: 0,
                                       left: -10,
                                       bottom: 0,
                                       right: 10)
        semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ?
            .forceLeftToRight : .forceRightToLeft
    }
}

