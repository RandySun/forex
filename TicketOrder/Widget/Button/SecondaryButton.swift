import UIKit

class SecondaryButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        self.isEnabled = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    private func setup() {
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: .callout)
        titleLabel?.adjustsFontForContentSizeCategory = true
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byWordWrapping
        layer.cornerRadius = 5
        layer.borderWidth = 2.0
        backgroundColor = .clear
        contentEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        clipsToBounds = false

        // Set initial background color based on enabled state.
        layer.borderColor = isEnabled ? UIColor(named: .primaryDark).cgColor : UIColor(named: .disabled).cgColor
        titleLabel?.textColor = isEnabled ? UIColor(named: .primaryDark) :  UIColor(named: .disabled)

        self.isEnabled = true
    }

    override var isEnabled: Bool {
        didSet {
            let color = isEnabled
                ? UIColor(named: .primaryDark)
                : UIColor(named: .disabled)
            layer.borderColor = color.cgColor
            setTitleColor(color, for: .normal)
        }
    }
}
