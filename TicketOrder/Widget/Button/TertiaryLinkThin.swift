import UIKit

class TertiaryLinkThin: UIButton {

    private func setup() {
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: .body)
        titleLabel?.adjustsFontForContentSizeCategory = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }

    convenience init() {
        self.init(type: .custom)
        titleLabel?.textColor = UIColor(named: .primaryLight)
        setup()
    }
}
