import UIKit

class TertiaryLink: UIButton {
    //Todo: This button doesn't support dark mode yet
    private func setup() {
        titleLabel?.font = ScaledFont.shared.font(forTextStyle: .callout)
        titleLabel?.adjustsFontForContentSizeCategory = true
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setTitle(title: String) {
        setTitle(title, for: .normal)
        sizeToFit()
    }
}

