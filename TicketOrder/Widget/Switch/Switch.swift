//
//  Switch.swift
//  TeachersHealthiOS
//
//  Created by Randy Sun on 2/7/19.
//  Copyright © 2019 Ticket Order. All rights reserved.
//

import UIKit

class Switch: UISwitch {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        onTintColor = UIColor(named: .primaryDark)
    }
}
