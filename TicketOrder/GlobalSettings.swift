//
//  GlobalSettings.swift
//  TicketOrder
//
//  Created by Sun, Randy on 23/10/19.
//  Copyright © 2020 Sun, Randy. All rights reserved.
//

import Foundation

enum GlobalBackgroundMode {
    case light,
    dark
}

class GlobalSettings {
    static var background = GlobalBackgroundMode.dark
}

