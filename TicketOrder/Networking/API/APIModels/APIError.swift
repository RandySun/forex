import Foundation
import SwiftyJSON

public enum ErrorType {
    case
    noNetwork,
    wrongCredentials,
    accountLocked,
    serializeError,
    internalError,
    serverError,    // 500
    nullData,
    badRequest,      // 400
    unauthorized,    // 401
    forbidden,       // 403
    notFound,                       // 404
    networkError,
    timeout,
    unknownError,                   // Catch all
    insecureConnection              // Thrown when NSURLSession detects security related network problems
}
/// An error type used by `APIResponse` for the usual types of errors.
         //408
public struct APIError: Error {
    let type: ErrorType
    let details: LinkError?
    
    func getErrorMessage()->(String, String){
        
        let detail = details?.description
        
        switch type {
        case .serializeError:
            return ("Error", detail ?? "An error has occurred. Please contact us.")
        case .serverError:
            return ("Server error", detail ?? "An error has occurred. Please try again.")
        case .badRequest:
            return ("Bad Request", detail ?? "The request could not be processed, please check the data entered.")
        case .unauthorized:
            return ("Unauthorised", detail ?? "These credentials are invalid, or you are not authorised to view this content. Please try again with valid credentials.")
        case .notFound:
            return ("Not found", detail ?? "The page you tried to find does not exist.")
        case .timeout:
            return ("Timeout", detail ?? "An error has occurred. Please try again.")
        case .accountLocked:
            return ("Login unsuccessful", "An error has occurred. Please try again.")
        case .wrongCredentials:
            return ("Please try again", detail ?? "Your details were incorrect.")
        default:
            return ("There was a problem", detail ?? "An error has occurred. Please try again.")
        }
    }
}

/// API Error Model for all non-auth and auth APIs
public struct APIErrorModel: Codable {
    
    /// Error string
    let message: String?
    
    /// Error code
    var code: String?
    let type: String?

    /// Details
    var details: String?
    
}

struct APIReponse: Decodable {
    let message: String?
    let errors: LinkError?//[LinkError]?
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case errors
    }
}

public struct LinkError: Decodable {
    let type: String?
    let description: String?
    let code: String?
}

public struct LoginError: Decodable {
    let Id: String?
    let Code: String
    let Description: String?
}



