import Foundation

/// Provides a serialized object, json or otherwise generic depending on the method
/// This API response type is used for convenience to provide the success value, or the error.
public enum APIResponse<T> where T: Codable {
    case
    success(_: T),
    failure(_: APIError)
}

struct UnusedResponse: Codable {
    
}

struct BoolResponse: Codable {
    let data: Bool
}

struct ErrorResponse: Decodable {
    let response: APIReponse?
    let error: String?
    let description: String?
    enum CodingKeys: String, CodingKey {
        case response = "ApiResponse"
        case description = "error_description"
        case error = "error"
    }
    var loginError: LoginError? {
        if let description = description, let data = description.data(using: .utf8) {
            return try? JSONDecoder().decode(LoginError.self, from: data)
        }
        return nil
    }
}
