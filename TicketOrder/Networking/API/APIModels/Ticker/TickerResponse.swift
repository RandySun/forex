//
//  GrantTypes.swift
//  LinkProject
//
//  Created by Randy Sun on 5/11/18.
//  Copyright © 2018 Deloitte Digital. All rights reserved.
//

import Foundation

enum Currency: String, CaseIterable {
    
    case USD,
    EUR,
    GBP,
    NZD,
    CNY,
    AED,
    ARS,
    BDT,
    BND,
    BRL,
    CAD,
    CHF,
    CLP,
    DKK,
    FJD,
    HKD,
    IDR,
    INR,
    JPY,
    KRW,
    LKR,
    MYR,
    NOK,
    PGK,
    PHP,
    PKR,
    SAR,
    SBD,
    SEK,
    SGD,
    THB,
    TOP,
    TWD,
    VND,
    VUV,
    WST,
    XPF,
    ZAR,
    XAU,
    CNH,
    noResult
    
    static func getCurrency(currency: String) -> Currency {
        guard Currency.allCases.filter( {
            return $0.rawValue == currency
        }).count == 1 else {
            return Currency.noResult
        }
        return Currency(rawValue: currency) ?? Currency.noResult
    }
    
    static func getAllCurrencies() -> [Currency] {
        return Currency.allCases
    }
}


struct RateDetail: Codable {
    
    let currencyCode: String
    let currencyName: String
    let country: String
    let buyTT: String
    let sellTT: String
    let buyTC: String
    let buyNotes: String
    let sellNotes: String
    let spotRateDateFmt: String
    let effectiveDateFmt: String
    let updateDateFmt: String
    let lastUpdated: String
    
    enum CodingKeys: String, CodingKey {
        case currencyCode,
        currencyName,
        country,
        buyTT,
        sellTT,
        buyTC,
        buyNotes,
        sellNotes,
        spotRateDateFmt = "SpotRate_Date_Fmt",
        effectiveDateFmt = "effectiveDate_Fmt",
        updateDateFmt = "updateDate_Fmt",
        lastUpdated = "LASTUPDATED"
        
    }
    
}

struct TickerResponse: Codable {
    
    let apiVersion: String
    let data: Brand
    
}

struct Brand: Codable {
    let brands: Bank
    enum CodingKeys: String, CodingKey {
        case brands = "Brands"
    }
}

struct Bank: Codable {
    let wbc: WestPac
    enum CodingKeys: String, CodingKey {
        case wbc = "WBC"
    }
}

struct WestPac: Codable {
    let brand: String
    let portfolios: FX
    enum CodingKeys: String, CodingKey {
        case brand = "Brand",
        portfolios = "Portfolios"
    }
}

struct FX: Codable {
    let fx: Portfolio
    enum CodingKeys: String, CodingKey {
        case fx = "FX"
    }
}

struct Portfolio: Codable {
    let portfolioId: String
    let products: CurrencyProduct
    enum CodingKeys: String, CodingKey {
        case portfolioId = "PortfolioId",
        products = "Products"
    }
}

struct CurrencyProduct: Codable {
    
    let USD: Product
    let EUR: Product
    let GBP: Product
    let NZD: Product
    let CNY: Product
    let AED: Product
    let ARS: Product
    let BDT: Product
    let BND: Product
    let BRL: Product
    let CAD: Product
    let CHF: Product
    let CLP: Product
    let DKK: Product
    let FJD: Product
    let HKD: Product
    let IDR: Product
    let INR: Product
    let JPY: Product
    let KRW: Product
    let LKR: Product
    let MYR: Product
    let NOK: Product
    let PGK: Product
    let PHP: Product
    let PKR: Product
    let SAR: Product
    let SBD: Product
    let SEK: Product
    let SGD: Product
    let THB: Product
    let TOP: Product
    let TWD: Product
    let VND: Product
    let VUV: Product
    let WST: Product
    let XPF: Product
    let ZAR: Product
    let XAU: Product
    let CNH: Product
    
    
    public func getTicket(by currency: Currency) -> Product? {
        switch currency {
        case .USD:
            return USD
        case .EUR:
            return EUR
        case .GBP:
            return GBP
        case .NZD:
            return NZD
        case .CNY:
            return CNY
        case .ARS:
            return ARS
        case .BDT:
            return BDT
        case .BND:
            return BND
        case .BRL:
            return BRL
        case .CAD:
            return CAD
        case .CHF:
            return CHF
        case .CLP:
            return CLP
        case .DKK:
            return DKK
        case .FJD:
            return FJD
        case .HKD:
            return HKD
        case .IDR:
            return IDR
        case .INR:
            return INR
        case .JPY:
            return JPY
        case .KRW:
            return KRW
        case .LKR:
            return LKR
        case .MYR:
            return MYR
        case .NOK:
            return PGK
        case .PGK:
            return NOK
        case .PHP:
            return PHP
        case .PKR:
            return PKR
        case .SAR:
            return SAR
        case .SBD:
            return SBD
        case .SEK:
            return SEK
        case .SGD:
            return SGD
        case .THB:
            return THB
        case .TOP:
            return TOP
        case .TWD:
            return TWD
        case .VND:
            return VND
        case .VUV:
            return VUV
        case .WST:
            return WST
        case .XPF:
            return XPF
        case .ZAR:
            return ZAR
        case .XAU:
            return XAU
        case .CNH:
            return CNH
        default:
            return USD
        }
    }
    
    public func getCurrencyCount() -> Int {
        return 40 //totoal number based on current response
    }
}

struct Product: Codable {
    let productId: String
    let rates: [String: RateDetail]
    
    enum CodingKeys: String, CodingKey {
        case productId = "ProductId",
        rates = "Rates"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        productId = try container.decode(String.self, forKey: .productId)
        rates = try container.decode([String: RateDetail].self, forKey: .rates)
    }
    
    public func getRateDetail(by currency: Currency) -> RateDetail? {
        return rates[currency.rawValue]
    }
}








