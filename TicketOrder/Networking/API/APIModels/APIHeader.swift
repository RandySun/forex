
import Foundation

class LoginHeader {
    
    static let header: [String: String] = [
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent" :"Test",
        "Cache-Control" : "no-cache, no-store, max-age=0, must-revalidate"
    ]
    
    
    static let clientBody: [String: String] = [
        "grant_type": "client_credentials",
        "client_secret": "clientSecret",
        "client_id": "test",
        "scope": "test"
    ]
}

class URLCreator {

    private static let baseClientUrl = "https://www.westpac.com.au/bin/getJsonRates.wbc.fx.json"
    
    static public func getTickerUrl()->String{
        return baseClientUrl
    }
     

}
