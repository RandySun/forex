//
//  DataWrapped.swift
//  LinkProject
//
//  Created by Randy Sun on 7/5/18.
//  Copyright © 2018 Deloitte Digital. All rights reserved.
//

import Foundation

struct DataWrapped<T: Codable>: Codable {
    let data: T
}
