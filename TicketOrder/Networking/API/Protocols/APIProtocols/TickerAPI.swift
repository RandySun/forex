//
//  RegistrationAPI.swift
//  LinkProject
//
//  Created by Randy Sun on 24/1/18.
//  Copyright © 2018 Deloitte Digital. All rights reserved.
//

import Foundation
import Alamofire

protocol TickerAPI {
    
    func getTicker(callback: @escaping (APIResponse<TickerResponse>) -> Void)
}

extension TickerAPI {
    
    func getTicker(callback: @escaping (APIResponse<TickerResponse>) -> Void) {
        
        let url = URLCreator.getTickerUrl()
        NetworkAPIInterface.shared.request(
            url,
            method: .get,
            parameters: [:],
            headers: [:],
            completion: callback
        )
        
    }

}

