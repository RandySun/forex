import Foundation
import Alamofire

public class NetworkAPI:
    TickerAPI
{
    
    
    /// Singleton
    static let shared = NetworkAPI()

    
    //-------------------------------
    
    var cachelessconfiguraton: URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        return configuration
    }
    
    /// Alamofire Manager
    private var _sessionManager: SessionManager?
    var alamofireManager : SessionManager {
        if let sm = _sessionManager {
            return sm
        } else {
            let sm = SessionManager(configuration: cachelessconfiguraton, serverTrustPolicyManager: ServerTrustPolicyManager(
                policies: NetworkAPI.getServerTrustPolicies()
            ))
            _sessionManager = sm
            return sm
        }
    }

    func clearInMemoryStorage() {
        
        alamofireManager.session.getAllTasks { (tasks) in
            tasks.forEach { $0.cancel() }
        }
        
    }
 
    /// Prevent intialisation except singleton
    private init() {}
    
    private static func getServerTrustPolicy(certificateName : String, pinningOnlyPublicKey:Bool = false)->ServerTrustPolicy?{
        return nil
    }
    
    private static func getServerTrustPolicies()->[String : ServerTrustPolicy]{
        let policies = [String : ServerTrustPolicy]()
        return policies
    }

}
