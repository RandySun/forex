import Foundation
import Alamofire
import SwiftyJSON

/// You should not need to use this except from an API singleton
class NetworkAPIInterface {
    
    /// Singleton
    static let shared = NetworkAPIInterface()
    /// Prevent initialisation except through singleton
    private init() {}
    
    /**
     A function that converts URLErrors into APIerrors.
     - Parameters:
     - error: The URL error to map
     - returns: An associated APIError
     */
    static func convertURLError(error: URLError) -> APIError {
        
        switch error.code {
        case .appTransportSecurityRequiresSecureConnection,
             .secureConnectionFailed,
             .serverCertificateUntrusted,
             .serverCertificateHasBadDate,
             .serverCertificateNotYetValid,
             .serverCertificateHasUnknownRoot:
            
            return APIError(type: .insecureConnection, details: nil)
            
        default:
            return APIError(type: .networkError, details: nil)
        }
    }
    
    /** 
     A function that converts Alamofire errors into APIerrors.
     - Parameters:
     - error: The Alamofire error to map
     - returns: An associated APIError
     */
    static func convertAFError(error: AFError) -> APIError {
        
        debugPrint("*** Converting Alamofire Error to API Error...")
        
        switch error {
        case .invalidURL(let url):
            debugPrint("Invalid URL: \(url) - \(error.localizedDescription)")
            return APIError(type: .networkError, details: nil)
            
        case .parameterEncodingFailed(let reason):
            debugPrint("Parameter encoding failed: \(error.localizedDescription)")
            debugPrint("Failure Reason: \(reason)")
            return APIError(type: .serializeError, details: nil)
            
        case .multipartEncodingFailed(let reason):
            debugPrint("Multipart encoding failed: \(error.localizedDescription)")
            debugPrint("Failure Reason: \(reason)")
            return APIError(type: .serializeError, details: nil)
            
        case .responseValidationFailed(let reason):
            debugPrint("Response validation failed: \(error.localizedDescription)")
            debugPrint("Failure Reason: \(reason)")
            
            switch reason {
            case .dataFileNil, .dataFileReadFailed:
                debugPrint("Downloaded file could not be read")
            case .missingContentType(let acceptableContentTypes):
                debugPrint("Content Type Missing: \(acceptableContentTypes)")
            case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                debugPrint("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
            case .unacceptableStatusCode(let code):
                debugPrint("Response status code was unacceptable: \(code)")
            }
            
            return APIError(type: .serializeError, details: nil)
            
        case .responseSerializationFailed(let reason):
            debugPrint("Response serialization failed: \(error.localizedDescription)")
            debugPrint("Failure Reason: \(reason)")
            return APIError(type: .serializeError, details: nil)
        }
        //debugPrint("Underlying error: \(String(describing: error.underlyingError))")
    }
    
    /**
     An interface to the underlying CA SDK / Other actual transport layer. This also inserts some default
     parameters used by Sandbox SDK. This will need to be updated if future environments are
     different.
     
     - Parameters:
     - Path: The url endpoint
     - method: The HTTP method to use
     - parameters: A dictionary of any additional parameters usually a serialised request object
     - headers: Any additional headers
     - encoding: The encoding, defaults to URL for `.get` but `JSONEncoding.default` for `.post`, `.put`
     - completion: The callback, returning a dictionary of results or `APIError`. This is not a
     swift-like API and should be changed if we no longer use the CA SDK, which enforces this style
     */
    func request<T>(
        _ path: String,
        method: HTTPMethod,
        parameters: [String: Any],
        headers: [String: String],
        encoding: ParameterEncoding = URLEncoding.default,
        completion: @escaping (APIResponse<T>) -> Void
        ) {
        
            guard let _path = URL(string: path) else {
                completion(.failure(APIError(type: .internalError, details: nil)))
                return
            }
        
            NetworkSDK.api.alamofireManager.request(
                _path,
                method: method,
                parameters: parameters,
                encoding: encoding,
                headers: headers
            )
            //.validate(statusCode: validStatusCodes)
            .responseData { response in
                self.handleResponse(
                    response: response,
                    completion: completion
                )
        }
    }

    /// Response handling
    /// Inspects a response and applies response handling rules
    func handleResponse<T>(
        response: DataResponse<Data>,
        completion: @escaping ((APIResponse<T>) -> Void)
    ) {
        
        switch response.result {
            
        case .success(let res):
            
            // Respond based on status code
            //Todo: we need to show server returns errors sometime,
            // we need to decode server error response and get back to called
            guard let _response = response.response else {
                completion(.failure(APIError(type: .nullData, details: nil)))
                return
            }
            
            switch _response.statusCode {
            
            case 400...1000:
                do {
                    debugPrint("Server Error Response:::", String(data: response.value ?? Data(), encoding: .utf8) ?? "")
                    let error = try getError(code: _response.statusCode, value: res)
                    completion(APIResponse.failure(error ?? APIError(type: .unknownError, details: nil)))
                } catch {
                    completion(APIResponse.failure(APIError(type: .unknownError, details: nil)))
                }
            default:
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let model = try decoder.decode(T.self, from: res)
                    completion(APIResponse.success(model))
                    
                } catch let error {
                    
                    if let decodingError = error as? DecodingError {
                        debugPrint("Decoding Error server Response:::", String(data: response.value ?? Data(), encoding: .utf8) as Any)
                        debugPrint(decodingError)
                    }
                    
                    let _error = error as? APIError ?? APIError(type: .serializeError, details: nil)
                    completion(APIResponse.failure(_error))
                }
            }
            
        case .failure(let err):
            debugPrint("*** Error encountered in API: \(err)")
            if let httpURLResponse = response.response {
                completion(.failure(getError(statusCode :httpURLResponse.statusCode)))
            } else {
                completion(.failure(APIError(type: .serializeError, details: nil)))
            }
        }
    }

    
    func getError(statusCode : Int, message : String? = nil)->APIError{
        let error = LinkError(type: nil, description: message, code: "\(statusCode)")
        switch statusCode {
        case 1, 2: // Client Errors
            return APIError(type: .wrongCredentials, details: error)
        case 5, 8:
            return APIError(type: .accountLocked, details: error)
        case 400:
            return APIError(type: .badRequest, details: error)
        case 401:
            return APIError(type: .unauthorized, details: error)
        case 403:
            return APIError(type: .forbidden, details: error)
        case 404:
            return APIError(type: .notFound, details: error)
        case 500: // Server Errors
            return APIError(type: .serverError, details: error)
        case 502:
            return APIError(type: .serverError, details: error)
        case 503:
            return APIError(type: .serverError, details: error)
        default:
            return APIError(type: .serializeError, details: error)
        }
    }

    func getError(code: Int, value: Data)throws ->APIError? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let error = try decoder.decode(ErrorResponse.self, from: value)
        let err = error.response?.errors
        
        if error.loginError?.Code == "2" || error.loginError?.Code == "5" {
            return APIError(type: .wrongCredentials, details: err)
        } else if error.loginError?.Code == "8" {
            return APIError(type: .accountLocked, details: err)
        }
        
        switch code {
        case 500...1000:
            return APIError(type: .serverError, details: err)
        case 400:
            return APIError(type: .badRequest, details: err)
        case 401..<404:
            return APIError(type: .unauthorized, details: err)
        case 404..<408:
            return APIError(type: .notFound, details: err)
        case 408:
            return APIError(type: .timeout, details: err)
        case 409..<500:
            return APIError(type: .networkError, details: err)
        default:
            break
        }
        return nil
    }
}
