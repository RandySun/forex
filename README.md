# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for ticketOrder demo ###

## Introduction

This is initial version of simple demo

## Setup 

- Run pod install and open xcworkspace
- Alamonfire for network
- Using Nimble and other related framework for test purpose only 

## Features

* Supporting dark/light mode(ps: light mode not fully tested, maybe not working very good, changed in GlobalSettings.swift to check the update)
* Supporting large text( some label may not tested very well)
* Supporting voice over
* Supporting from iOS 11
* Pull to refresh to trigger the network call again
* Tap USD/AUD to change currency and buy/sell price, etc. Currently use buyTT/sellTT as main buy/sell price. Some currency doesn't have the correct buyTT/sellTT, for this kind of currency, simply shows NA. 
* Basic test cases added but very limited, code coverage will be very low since adding more test cases will need much more time, and also UI test needed. 

## Issues
* Error handled but not shown in alert
* Unit test should mock API and have a another set of test
* UI test cases should be added
* All string should move to localisation

## Suggestions
* API should be more flexible 


## Thanks

* Thanks so much for your time on reading this
